package marsrover;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class MarsRoversTest {
	
	@Test
	public void rebot_demare_position_0_0() {
		// GIVEN
		
		//WHEN
		MarsRovers robot = new MarsRovers();
		
		//THEN
		Assertions.assertThat(robot.position()).isEqualTo("0 0");
	}

}
