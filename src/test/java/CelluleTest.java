import org.assertj.core.api.Assertions;
import org.junit.Test;

import etatcellule.EnumEtat;
import etatcellule.EtatReproduction;
import etatcellule.EtatSousPopulation;
import etatcellule.EtatSurPopulation;
import etatcellule.EtatSurvie;

public class CelluleTest {

	@Test
	public void cellule_vivante_sans_voisins_vivants_meurt_par_sous_population(){
		// GIVEN
		Cellule cell = new CelluleVivante();
		cell.setEtatVoisinage(new EtatSousPopulation());
		
		// WHEN
		cell.jouerProchaineEtape();

		// THEN
		Assertions.assertThat(cell.getEtat()).isEqualTo(EnumEtat.Mort);
	}

	@Test
	public void cellule_vivante_avec_deux_voisins_vivants_reste_en_vie(){
		// GIVEN
		Cellule cell = new CelluleVivante();
		cell.setEtatVoisinage(new EtatSurvie());

		// WHEN
		cell.jouerProchaineEtape();

		// THEN
		Assertions.assertThat(cell.getEtat()).isEqualTo(EnumEtat.Vivant);
	}

	@Test
	public void cellule_vivante_avec_plus_de_trois_voisins_vivants_meurt(){
		// GIVEN
		Cellule cell = new CelluleVivante();
		cell.setEtatVoisinage(new EtatSurPopulation());

		// WHEN
		cell.jouerProchaineEtape();

		// THEN
		Assertions.assertThat(cell.getEtat()).isEqualTo(EnumEtat.Mort);
	}

	@Test
	public void cellule_morte_avec_trois_voisins_vivants_nait_par_reproduction(){
		// GIVEN
		Cellule cell = new CelluleMorte();
		cell.setEtatVoisinage(new EtatReproduction());

		// WHEN
		cell.jouerProchaineEtape();

		// THEN
		Assertions.assertThat(cell.getEtat()).isEqualTo(EnumEtat.Vivant);
	}

	@Test
	public void cellule_morte_avec_deux_voisins_reste_morte(){
		// GIVEN
		Cellule cell = new CelluleMorte();
		cell.setEtatVoisinage(new EtatSurvie());

		// WHEN
		cell.jouerProchaineEtape();

		// THEN
		Assertions.assertThat(cell.getEtat()).isEqualTo(EnumEtat.Mort);
	}




}
