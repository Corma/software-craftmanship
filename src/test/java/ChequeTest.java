import org.assertj.core.api.Assertions;
import org.junit.Test;

public class ChequeTest {

	@Test
	public void return_zero_when_ask_0() {
		// GIVEN

		// WHEN
		String numeroConverti = Cheque.convert(0);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("zero");
	}
	
	@Test
	public void return_un_when_ask_1() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(1);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("un");		
	}
	
	@Test
	public void return_deux_when_ask_2() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(2);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("deux");		
	}
	
	@Test
	public void return_trois_when_ask_3() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(3);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("trois");		
	}
	
	@Test
	public void return_quatre_when_ask_4() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(4);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("quatre");		
	}
	
	@Test
	public void return_seize_when_ask_16() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(16);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("seize");		
	}
	
	@Test
	public void return_dix_sept_when_ask_17() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(17);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("dix-sept");		
	}
	
	@Test
	public void return_dix_huit_when_ask_18() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(18);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("dix-huit");		

	}

	@Test
	public void return_dix_neuf_when_ask_19() {
		// GIVEN

		// WHEN
		String numeroConverti = Cheque.convert(19);

		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("dix-neuf");

	}

	@Test
	public void return_vingt_when_ask_20() {
		// GIVEN
		
		// WHEN
		String numeroConverti = Cheque.convert(20);
		
		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("vingt");		
	}

	@Test
	public void return_vingt_et_un_when_ask_21() {
		// GIVEN

		// WHEN
		String numeroConverti = Cheque.convert(21);

		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("vingt et un");
	}

	@Test
	public void return_vingt_deux_when_ask_22() {
		// GIVEN

		// WHEN
		String numeroConverti = Cheque.convert(22);

		//THEN
		Assertions.assertThat(numeroConverti).isEqualTo("vingt-deux");
	}



}
