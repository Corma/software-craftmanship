import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import etatcellule.EnumEtat;

public class GrilleTest {
	
	@Test
	public void une_grille_de_cellule_en_sous_population_reste_morte() {
		// GIVEN
		Grille grille = new Grille();
		Cellule[] tableauEntree = new Cellule[9];
		for(int i =0; i<tableauEntree.length; i++) {
			tableauEntree[i] = new CelluleMorte();
		}
		grille.initialisaterGrille(tableauEntree);
		
		// WHEN
		grille.jouerProchaineEtape();
		
		// THEN
		Cellule[] tableauGrille = grille.getCellules();
		for(int i =0; i<tableauGrille.length; i++) {
			Assertions.assertThat(tableauGrille[i].getEtat()).isEqualTo(EnumEtat.Mort);
		}
		
		
	}
	
	@Test
	public void une_grille_de_cellule_en_surpopulation_devient_morte_sauf_dans_les_coins() {
		// GIVEN
		Grille grille = new Grille();
		Cellule[] tableauEntree = new Cellule[9];
		for(int i =0; i<tableauEntree.length; i++) {
			tableauEntree[i] = new CelluleVivante();
		}
		grille.initialisaterGrille(tableauEntree);
		
		// WHEN
		grille.jouerProchaineEtape();
		
		// THEN
		Cellule[] tableauGrille = grille.getCellules();
		Assertions.assertThat(tableauGrille[0].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[1].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[2].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[3].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[4].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[5].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[6].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[7].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[8].getEtat()).isEqualTo(EnumEtat.Vivant);
		
		
	}
	
	
	@Test
	public void une_grille_de_cellule_en_surpopulation_devient_morte_en_deux_tours() {
		// GIVEN
		Grille grille = new Grille();
		Cellule[] tableauEntree = new Cellule[9];
		for(int i =0; i<tableauEntree.length; i++) {
			tableauEntree[i] = new CelluleVivante();
		}
		grille.initialisaterGrille(tableauEntree);
		// 0 0 0
		// 0 0 0
 		// 0 0 0
		
		// WHEN
		grille.jouerProchaineEtape();
		// 0 X 0
		// X X X
 		// 0 X 0
		grille.jouerProchaineEtape();
		// X X X
		// X X X
 		// X X X
		
		// THEN
		Cellule[] tableauGrille = grille.getCellules();
		Assertions.assertThat(tableauGrille[0].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[1].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[2].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[3].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[4].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[5].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[6].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[7].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[8].getEtat()).isEqualTo(EnumEtat.Mort);
		
		
	}
	
	@Test
	public void une_croix_de_cellule_vivante_rend_tout_vivant_sauf_le_centre() {
		// GIVEN
		// 0 X 0
		// X X X
 		// 0 X 0
		Grille grille = new Grille();
		Cellule[] tableauEntree = new Cellule[9];
		tableauEntree[0] = new CelluleMorte();
		tableauEntree[1] = new CelluleVivante();
		tableauEntree[2] = new CelluleMorte();
		tableauEntree[3] = new CelluleVivante();
		tableauEntree[4] = new CelluleVivante();
		tableauEntree[5] = new CelluleVivante();
		tableauEntree[6] = new CelluleMorte();
		tableauEntree[7] = new CelluleVivante();
		tableauEntree[8] = new CelluleMorte();
		grille.initialisaterGrille(tableauEntree);
		
		// WHEN
		grille.jouerProchaineEtape();
		
		// THEN
		// X X X
		// X 0 X
 		// X X X
		Cellule[] tableauGrille = grille.getCellules();
		Assertions.assertThat(tableauGrille[0].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[1].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[2].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[3].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[4].getEtat()).isEqualTo(EnumEtat.Mort);
		Assertions.assertThat(tableauGrille[5].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[6].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[7].getEtat()).isEqualTo(EnumEtat.Vivant);
		Assertions.assertThat(tableauGrille[8].getEtat()).isEqualTo(EnumEtat.Vivant);
		
		
	}
	
	

}
