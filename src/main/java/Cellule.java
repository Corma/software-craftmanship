import etatcellule.EnumEtat;
import etatcellule.EtatReproduction;
import etatcellule.EtatSousPopulation;
import etatcellule.EtatSurPopulation;
import etatcellule.EtatSurvie;
import etatcellule.EtatVoisinage;

public abstract class Cellule {

	protected EnumEtat etat;
	protected EtatVoisinage etatVoisinage;

	public EnumEtat getEtat() {
		return etat;
	}

	
	public abstract void jouerProchaineEtape();


	public void setEtatVoisinage(EtatVoisinage etatVoisinage) {
		this.etatVoisinage = etatVoisinage;
	}


	public boolean isVivante() {
		return EnumEtat.Vivant.equals(etat);
	}


	public void estEnSousPopulation() {
		this.etatVoisinage = new EtatSousPopulation();
		
	}
	public void estEnSurvie() {
		this.etatVoisinage = new EtatSurvie();
		
	}


	public void estEnReproduction() {
		this.etatVoisinage = new EtatReproduction();
		
	}


	public void estEnSurpopulation() {
		this.etatVoisinage = new EtatSurPopulation();
		
	}




}
