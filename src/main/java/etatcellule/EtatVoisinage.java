package etatcellule;
public abstract class EtatVoisinage {

	public EnumEtat getEtatCelluleMorte() {
		return EnumEtat.Mort;
	};
	public abstract EnumEtat getEtatCelluleVivante();

	
}
