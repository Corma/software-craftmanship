package etatcellule;
public class EtatReproduction extends EtatVoisinage {

	@Override
	public EnumEtat getEtatCelluleVivante() {
		return EnumEtat.Vivant;
	}
	
	@Override
	public EnumEtat getEtatCelluleMorte() {
		return EnumEtat.Vivant;
	}

}
