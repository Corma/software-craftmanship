public class Cheque {

	private static final String ET = " et ";
	private static final String TIRET = "-";

	private final static String[] tabUnite = new String[]{"zero", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf", "dix", "onze", "douze", "treize", "quatorze", "quinze", "seize"};

	private final static String[] tabDizaine = new String[]{"zero", "dix", "vingt", "trente", "quarante", "cinquante"};

	public static String convert(int i) {
		int dizaine = getDizaine(i);

		if(isMultipleDe10(i)){
			return convertirDizaine(dizaine);
		}else if(i>16) {
			if(i%20 == 1){
				return convertirDizaine(dizaine)+ET+convertirUnite(i%20);
			}
			return convertirDizaine(dizaine)+TIRET+convertirUnite(i%10);
		} else {
			return convertirUnite(i);
		}
	}

	private static boolean isMultipleDe10(int i) {
		return i%10==0;
	}

	private static int getDizaine(int i) {
		return (i/10)%10;
	}

	private static String convertirUnite(int i) {
		return tabUnite[i];
	}

	private static String convertirDizaine(int i) {
		return tabDizaine[i];
	}

}
