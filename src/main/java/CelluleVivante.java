import etatcellule.EnumEtat;

public  class CelluleVivante extends Cellule{
	
	public CelluleVivante() {
		super();
		this.etat = EnumEtat.Vivant;
	}
	
	@Override
	public void jouerProchaineEtape() {
		etat = etatVoisinage.getEtatCelluleVivante();
	}


}
