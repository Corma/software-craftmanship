import etatcellule.EnumEtat;

public  class CelluleMorte  extends Cellule{
	

	public CelluleMorte() {
		super();
		this.etat = EnumEtat.Mort;
	}

	@Override
	public void jouerProchaineEtape() {
		etat = etatVoisinage.getEtatCelluleMorte();
	}

}
