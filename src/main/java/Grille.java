import java.util.HashMap;
import java.util.Map;

public class Grille {
	
	private Cellule[] tableauGrille = new Cellule[9];
	Map<Integer, int[]> mappingCellule = new HashMap<>();
	
	public Grille() {
		creerMappingCelluleAvecSesVoisins();
	}
	
	public Cellule[] getCellules() {
		return tableauGrille;
	}

	private void creerMappingCelluleAvecSesVoisins() {
		mappingCellule.put(0,  new int[] {1, 3, 4});
		mappingCellule.put(1,  new int[] {0, 2, 3, 4, 5});
		mappingCellule.put(2,  new int[] {1, 4, 5});
		mappingCellule.put(3,  new int[] {0, 1, 4, 6, 7});
		mappingCellule.put(4,  new int[] {0, 1, 2, 3, 5, 6, 7, 8});
		mappingCellule.put(5,  new int[] {1, 2, 4, 7, 8});
		mappingCellule.put(6,  new int[] {3, 4, 7});
		mappingCellule.put(7,  new int[] {3, 4, 5, 6, 8});
		mappingCellule.put(8,  new int[] {4, 5, 7});
	}

	public void initialisaterGrille(Cellule[] tableauEntree) {
		for(int i =0; i<tableauEntree.length; i++) {
			tableauGrille[i] = tableauEntree[i];
		}
		
	}

	public void jouerProchaineEtape() {
		calculerVoisinage();
		for(int i =0; i<tableauGrille.length; i++) {
			Cellule cellule = tableauGrille[i];
			cellule.jouerProchaineEtape(); 
			// TODO REMOVE IF
			if(cellule.isVivante()) {
				tableauGrille[i] = new CelluleVivante();
			} else {
				tableauGrille[i] = new CelluleMorte();
			}
		}
		
	}

	private void calculerVoisinage() {
		for(int i =0; i<tableauGrille.length; i++) {
			Cellule cellule = tableauGrille[i];
			int nombreVoisinVivant =0;
			int[] voisinsCellule = mappingCellule.get(i);
			for(int j = 0; j < voisinsCellule.length; j++) {
				int numeroVoisin = voisinsCellule[j]; 
				Cellule celluleVoisine = tableauGrille[numeroVoisin];
				if(celluleVoisine.isVivante()) {
					nombreVoisinVivant++;
				}
			}
			
			// TODO REMOVE IF - 
			if(nombreVoisinVivant<2) {
				cellule.estEnSousPopulation();
			} else if(nombreVoisinVivant == 2) {
				cellule.estEnSurvie();

			} else if(nombreVoisinVivant == 3) {
				cellule.estEnReproduction();

			} else {
				cellule.estEnSurpopulation();

			}
				
			
		}
		
	}


}
